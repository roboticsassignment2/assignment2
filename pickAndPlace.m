%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Project           	  : Robotics Assignment 2
%
% Author           		  : Jorge Villanueva Salamero & Riccardo dalla
%
% Last Date Modified      : 06-07-2019
%
% Purpose           	  : Simulation of the Dobot Magician building a piramid of 6 cubes
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function pickAndPlace() 
clc 
close all

r = Dobot;
q = [0 5*pi/180 pi/2 pi/2 0];
r.model.animate(q);
%r.model.teach();

redCube1 =   [0.2 0.2 0];
redCube2 =   [0 0.3 0];
greenCube1 = [0.1 0.2 0];
greenCube2 = [0.15 0.25 0];
blueCube1 =  [0 0.2 0];
blueCube2 =  [0.1 0.25 0];

[h_red1,v_red1,cubeVertexCount_red1]        = plot1Cube(transl(redCube1), 'redCube.ply');
[h_red2,v_red2,cubeVertexCount_red2]        = plot1Cube(transl(redCube2), 'redCube.ply');

[h_green1,v_green1,cubeVertexCount_green1]   = plot1Cube(transl(greenCube1), 'greenCube.ply');
[h_green2,v_green2,cubeVertexCount_green2]   = plot1Cube(transl(greenCube2), 'greenCube.ply');

[h_blue1,v_blue1,cubeVertexCount_blue1]      = plot1Cube(transl(blueCube1), 'blueCube.ply');
[h_blue2,v_blue2,cubeVertexCount_blue2]      = plot1Cube(transl(blueCube2), 'blueCube.ply');

h_cubes              = [h_red1; h_red2; h_green1; h_green2; h_blue1; h_blue2];
vertex_counts_cubes  = [cubeVertexCount_red1; cubeVertexCount_red2; cubeVertexCount_green1; cubeVertexCount_green2; cubeVertexCount_blue1; cubeVertexCount_blue2];
v_cubes_aux = v_red1;

z_ofset_moving_cube     =   [ 0,    0,  0.05] ;
z_ofset_suction         =   [ 0,    0,   0.005 ] ;
z_ofset_dropping_L1     =   [ 0,    0,  0.02 ];
z_ofset_dropping_L2     =   [ 0,    0,  0.040 ];
z_ofset_moving_cube_L3  =   [ 0,    0,  0.095 ];
z_ofset_dropping_cube_L3 =  [ 0,    0,  0.06 ];

center_redCube1_moving      =   redCube1 +	 z_ofset_moving_cube ;
center_redCube1_dropping    =   redCube1 +	 z_ofset_suction ;
center_redCube2_moving      =   redCube2 +	 z_ofset_moving_cube ;
center_redCube2_dropping    =	redCube2 +	 z_ofset_suction ;
center_greenCube1_moving    =	greenCube1 +	 z_ofset_moving_cube ;
center_greenCube1_dropping  =	greenCube1 +	 z_ofset_suction ;
center_greenCube2_moving    =	greenCube2 +	 z_ofset_moving_cube ;
center_greenCube2_dropping  =	greenCube2 +	 z_ofset_suction ;
center_blueCube1_moving     =   blueCube1  +  z_ofset_moving_cube ;
center_blueCube1_dropping   =   blueCube1  +  z_ofset_suction ;
center_blueCube2_moving     =   blueCube2  +  z_ofset_moving_cube ;
center_blueCube2_dropping   =   blueCube2 +   z_ofset_suction ;

center_moving = [center_redCube1_moving; center_redCube2_moving; center_greenCube1_moving; center_greenCube2_moving; center_blueCube1_moving; center_blueCube2_moving];
center_dropping = [center_redCube1_dropping; center_redCube2_dropping; center_greenCube1_dropping; center_greenCube2_dropping; center_blueCube1_dropping; center_blueCube2_dropping];

% Level 1
final_redCube1_moving   =   [0.3    0   0]  +   z_ofset_moving_cube;
final_redCube1_dropping =   [0.3    0   0]  +   z_ofset_dropping_L1;
final_redCube2_moving   =   [0.28  0   0]  +   z_ofset_moving_cube;   
final_redCube2_dropping =   [0.28  0   0]  +   z_ofset_dropping_L1;    
final_greenCube1_moving =   [0.26  0   0]  +   z_ofset_moving_cube;    
final_greenCube1_dropping = [0.26  0   0]  +   z_ofset_dropping_L1;   

% Level 2
final_greenCube2_moving     = [0.27    0       0  ]  + z_ofset_moving_cube;
final_greenCube2_dropping   = [0.27    0       0  ]  + z_ofset_dropping_L2 ;
final_blueCube1_moving      = [0.29    0       0  ]  + z_ofset_moving_cube;
final_blueCube1_dropping    = [0.29    0       0  ]  + z_ofset_dropping_L2;

% Level 3
final_blueCube2_moving      = [0.28    0  0  ]  + z_ofset_moving_cube_L3;
final_blueCube2_dropping    = [0.28    0  0  ]  + z_ofset_dropping_cube_L3;


final_moving    = [ final_redCube1_moving; final_redCube2_moving; final_greenCube1_moving; final_greenCube2_moving; final_blueCube1_moving; final_blueCube2_moving];
final_dropping  = [ final_redCube1_dropping; final_redCube2_dropping; final_greenCube1_dropping; final_greenCube2_dropping; final_blueCube1_dropping; final_blueCube2_dropping];


plotImages();
paperPosition = transl(0.1,-0.3,0);
% plotPaper(paperPosition);

[f,v,data] = plyread('emergency.ply','tri');
buttonVertexCount = size(v,1);
buttonPose = transl(-0.5,0.5,0);
vertexColours = [data.vertex.red, data.vertex.green, data.vertex.blue,] / 255;
h = trisurf(f, v(:,1), v(:,2), v(:,3), 'FaceVertexCData', vertexColours);
updatedPoints = [buttonPose * [v, ones(buttonVertexCount,1)]']';
h.Vertices = updatedPoints(:,1:3);

[f,v,data] = plyread('fence.ply','tri');
buttonVertexCount = size(v,1);
fencePose = eye(4);
vertexColours = [data.vertex.red, data.vertex.green, data.vertex.blue,] / 255;
h = trisurf(f, v(:,1), v(:,2), v(:,3), 'FaceVertexCData', vertexColours);
updatedPoints = [fencePose * [v, ones(buttonVertexCount,1)]']';
h.Vertices = updatedPoints(:,1:3);
 
steps = 50;

[m,n] = size(final_moving)

for i = 1 : m     
    
    % Sizes are different, We could not find another way of doing this
    if ( i == 2 ); v_cubes_aux = v_red2;
    elseif ( i == 3 ); v_cubes_aux = v_green1;
    elseif ( i == 3 ); v_cubes_aux = v_green2;
    elseif ( i == 3 ); v_cubes_aux = v_blue1;
    elseif ( i == 4 ); v_cubes_aux = v_blue2;
    end
   
   % Go To Cube 
   q = dobotIkconSimulation( center_moving(i,:) );
   move_simulation_robot(q, r, steps);
   
   % Pick Cube
   q = dobotIkconSimulation(center_dropping(i,:));
   move_simulation_robot(q, r, steps);
   
   % Go up in the z axis, ( don't hit any other cube )
   q = dobotIkconSimulation( center_moving(i,:) );  
   move_simulation_robot_and_cube(q, r, steps, h_cubes(i), v_cubes_aux, vertex_counts_cubes(i));
   
   % Going to final position
   q = dobotIkconSimulation( final_moving(i,:) );  
   move_simulation_robot_and_cube(q, r, steps, h_cubes(i), v_cubes_aux, vertex_counts_cubes(i));
   
   %  Reach final position 
   q = dobotIkconSimulation( final_dropping(i,:) ); 
   move_simulation_robot_and_cube(q, r, steps, h_cubes(i), v_cubes_aux, vertex_counts_cubes(i));
   
   % Go up in the z axis, ( don't hit any other cube )
   q = dobotIkconSimulation( final_moving(i,:) );
   move_simulation_robot(q, r, steps);                           % Go up in the z axis, ( to don't hit any other cube )

end


end

