function plotImages()

hold on;      
img = imread('playground1.jpg');     % Load a sample image
xImage = [-0.6 -0.6; -0.6 -0.6]; % The x data for the image corners
yImage = [-0.6 0.6; -0.6 0.6];   % The y data for the image corners
zImage = [0.5 0.5; 0 0];   % The z data for the image corners
surf(xImage,yImage,zImage,...    % Plot the surface
     'CData',img,...
     'FaceColor','texturemap');
hold on;      
img = imread('playground2.jpg');    
xImage = [-0.6 0.6; -0.6 0.6];     
yImage = [-0.6 -0.6; -0.6 -0.6];    
zImage = [0.5 0.5; 0 0];   
surf(xImage,yImage,zImage,... 
     'CData',img,...
     'FaceColor','texturemap');
 hold on;  
 img = imread('gravel.png');    
xImage = [-0.6 0.6; -0.6 0.6];  
yImage = [-0.6 -0.6; 0.6 0.6];      
zImage = [0 0; 0 0];  
surf(xImage,yImage,zImage,... 
     'CData',img,...
     'FaceColor','texturemap');
 hold on;  
 
end