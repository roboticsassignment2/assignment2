function plot6Cubes(redCube1Pose,redCube2Pose, greenCube1Pose, greenCube2Pose, blueCube1Pose, blueCube2Pose)

hold on;
[f,v,data] = plyread('redCube.ply','tri');
cubeVertexCount = size(v,1);
vertexColours = [data.vertex.red, data.vertex.green, data.vertex.blue,] / 255;
h = trisurf(f, v(:,1), v(:,2), v(:,3), 'FaceVertexCData', vertexColours);
updatedPoints = [redCube1Pose * [v, ones(cubeVertexCount,1)]']';
h.Vertices = updatedPoints(:,1:3);

hold on;
[f,v,data] = plyread('greenCube.ply','tri');
cubeVertexCount = size(v,1);
vertexColours = [data.vertex.red, data.vertex.green, data.vertex.blue,] / 255;
h = trisurf(f, v(:,1), v(:,2), v(:,3), 'FaceVertexCData', vertexColours);
updatedPoints = [greenCube1Pose * [v, ones(cubeVertexCount,1)]']';
h.Vertices = updatedPoints(:,1:3);

hold on;
[f,v,data] = plyread('blueCube.ply','tri');
cubeVertexCount = size(v,1);
vertexColours = [data.vertex.red, data.vertex.green, data.vertex.blue,] / 255;
h = trisurf(f, v(:,1), v(:,2), v(:,3), 'FaceVertexCData', vertexColours);
updatedPoints = [blueCube1Pose * [v, ones(cubeVertexCount,1)]']';
h.Vertices = updatedPoints(:,1:3);

hold on;
[f,v,data] = plyread('redCube.ply','tri');
cubeVertexCount = size(v,1);
vertexColours = [data.vertex.red, data.vertex.green, data.vertex.blue,] / 255;
h = trisurf(f, v(:,1), v(:,2), v(:,3), 'FaceVertexCData', vertexColours);
updatedPoints = [redCube2Pose * [v, ones(cubeVertexCount,1)]']';
h.Vertices = updatedPoints(:,1:3);

hold on;
[f,v,data] = plyread('greenCube.ply','tri');
cubeVertexCount = size(v,1);
vertexColours = [data.vertex.red, data.vertex.green, data.vertex.blue,] / 255;
h = trisurf(f, v(:,1), v(:,2), v(:,3), 'FaceVertexCData', vertexColours);
updatedPoints = [greenCube2Pose * [v, ones(cubeVertexCount,1)]']';
h.Vertices = updatedPoints(:,1:3);

hold on;
[f,v,data] = plyread('blueCube.ply','tri');
cubeVertexCount = size(v,1);
vertexColours = [data.vertex.red, data.vertex.green, data.vertex.blue,] / 255;
h = trisurf(f, v(:,1), v(:,2), v(:,3), 'FaceVertexCData', vertexColours);
updatedPoints = [blueCube2Pose * [v, ones(cubeVertexCount,1)]']';
h.Vertices = updatedPoints(:,1:3);

end
