function q = dobotIkconReal( position, jangsvc_, rotationQ4_cube)

jangmsg_ = rosmessage(jangsvc_);

a1_v1 = 0.132;
a2_v1 = 0.137;
a3_v1 = 0.168;
a4_v1 = 0.059;
a5_v1 = 0.055;

x_v1 = position(1) ;
y_v1 = position(2) ;
z_v1 = position(3) + a5_v1 + 0.05 - a1_v1;
q1 = atan(-x_v1/y_v1);

xy_my_length = sqrt ( (x_v1^2) + (y_v1^2) )  - a4_v1;

x_v1 = cos(q1) * xy_my_length ;
y_v1 = sin(q1) * xy_my_length;

syms x y z q2 q3 a2 a3;

f1 = a2*sin(q2)+a3*cos(q3) == sqrt(x^2+y^2)
f2 = a2*cos(q2)-a3*sin(q3) == z
solution = solve([f1,f2],[q2,q3]);
solution.q2;
solution.q3;

q2 = eval (  subs(solution.q2(2),{x,y,z,a2,a3},{x_v1,y_v1,z_v1,a2_v1,a3_v1}) );
q3 = eval ( subs(solution.q3(2),{x,y,z,a2,a3},{x_v1,y_v1,z_v1,a2_v1,a3_v1}) );
q4 = (pi/2) - (q3);

q = [q1 q2 q3 q4 0];

jangmsg_.TargetPoints =  [ q(1) q(2) q(3) (q(4)+ rotationQ4_cube) ];
jangsvc_.call(jangmsg_);

end