function  pickAndPlaceRealIkconObjectRecognition()

clc 
close all

z_ofset_moving_cube     =    0.035 ;
z_ofset_suction         =   -0.054 ;

z_ofset_dropping_L1     =   [ 0,    0,  -0.055 ];
z_ofset_dropping_L2     =   [ 0,    0,  -0.016 ];
z_ofset_moving_cube_L3  =   [ 0,    0,  0.045 ];
z_ofset_dropping_cube_L3 =  [ 0,    0,  0.007 ];

[distance_x, distance_y] = detect_v2();

% Level 1
final_redCube1_moving   =   [0.288  0   0]  +   z_ofset_moving_cube;
final_redCube1_dropping =   [0.288  0   0]  +   z_ofset_dropping_L1;
final_redCube2_moving   =   [0.252  0   0]  +   z_ofset_moving_cube;   
final_redCube2_dropping =   [0.252  0   0]  +   z_ofset_dropping_L1;    
final_greenCube1_moving =   [0.220  0   0]  +   z_ofset_moving_cube;    
final_greenCube1_dropping = [0.220  0   0]  +   z_ofset_dropping_L1;   

% Level 2
final_greenCube2_moving     = [0.245    0       0       ]  + z_ofset_moving_cube;
final_greenCube2_dropping   = [0.245    0       0       ]  + z_ofset_dropping_L2 ;
final_blueCube1_moving      = [0.280    0       0       ]  + z_ofset_moving_cube;
final_blueCube1_dropping    = [0.280    0       0       ]  + z_ofset_dropping_L2;

% Level 3
final_blueCube2_moving      = [0.265    0  0  ]  + z_ofset_moving_cube_L3;
final_blueCube2_dropping    = [0.265    0  0  ]  + z_ofset_dropping_cube_L3;

final_moving    = [ final_redCube1_moving; final_redCube2_moving; final_greenCube1_moving; final_greenCube2_moving; final_blueCube1_moving; final_blueCube2_moving];
final_dropping  = [ final_redCube1_dropping; final_redCube2_dropping; final_greenCube1_dropping; final_greenCube2_dropping; final_blueCube1_dropping; final_blueCube2_dropping];

%rosinit('http://10.42.0.1:11311')

suctioncupsvc_ = rossvcclient('/dobot_magician/end_effector/set_suction_cup');
suctioncupmsg_ = rosmessage(suctioncupsvc_);

cartsvc_ = rossvcclient('/dobot_magician/PTP/set_cartesian_pos'); cartmsg_ = rosmessage(cartsvc_);
jangsvc_ = rossvcclient('/dobot_magician/PTP/set_joint_angles'); jangmsg_ = rosmessage(jangsvc_);

suctioncupmsg_.IsEndEffectorEnabled=1;
suctioncupmsg_.EndEffectorState=1;
suctioncupsvc_.call(suctioncupmsg_);

% Red 1
[m,n] = size(distance_x)
for i = 1 : n 
    dobotIkconReal( [ distance_x(1,i) distance_y(1,i) z_ofset_moving_cube], jangsvc_, 0);     % Go To Cube 
    suctioncupmsg_.EndEffectorState=1; suctioncupsvc_.call(suctioncupmsg_);
    dobotIkconReal( [ distance_x(1,i) distance_y(1,i) z_ofset_suction], jangsvc_, 0);                              % Pick Cube
    pause(3);                                                                           % Activate suction
    dobotIkconReal( [ distance_x(1,i) distance_y(1,i) z_ofset_moving_cube], jangsvc_, 0);     % Go To Cube 
   
    q = dobotIkconReal(final_moving(i,:), jangsvc_, 0);                         % Going to final position &  Adjust Rotation of the cube

    q = dobotIkconReal(final_dropping(i,:), jangsvc_, -2.6);                          %  Reaching final position & Adjust Rotation of the cube

    pause(3);   suctioncupmsg_.EndEffectorState=0; suctioncupsvc_.call(suctioncupmsg_);  pause(1); % Desactivate suction
    dobotIkconReal(final_moving(i,:), jangsvc_,0);                              % Go up in the z axis, ( to don't hit any other cube )


end

