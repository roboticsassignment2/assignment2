classdef Dobot < handle
    properties
        model;
        workspace = [-0.5 0.5 -0.5 0.5 0 0.5];   
    end
    
    methods
        function self = Dobot()
            self.GetDobot();
            self.PlotAndColourRobot();
            drawnow 
        end

        function GetDobot(self)
            pause(0.001);
            
            name = ['Dobot'];
            L0 = Link('d', 0.132,'a', 0,'alpha', -pi/2, 'qlim', deg2rad([-135 135]), 'offset', pi/2);
            L1 = Link('d', 0,'a', 0.137,'alpha', 0,'offset', -pi/2, 'qlim', deg2rad([5 80]));
            L2 = Link('d', 0,'a', 0.168,'alpha', 0, 'qlim', deg2rad([15 170]));
            L3 = Link('d', 0,'a', 0.059,'alpha', pi/2, 'qlim',[-pi/2 pi/2],'offset', -pi/2);
            L4 = Link('d',-0.055,'a',0,'alpha',0,'qlim',deg2rad([-90 90]),'offset', pi/2);

            self.model = SerialLink([L0 L1 L2 L3 L4],'name',name);
%             self.model.plot(zeros(1,5));
        end

        %% PlotAndColourRobot
        % Given a robot index, add the glyphs (vertices and faces) and
        % colour them in if data is available 
        function PlotAndColourRobot(self)%robot,workspace)
            for linkIndex = 0:self.model.n
                [ faceData, vertexData, plyData{linkIndex + 1} ] = plyread(['DobotLink',num2str(linkIndex),'.ply'],'tri'); %#ok<AGROW>
                self.model.faces{linkIndex + 1} = faceData;
                self.model.points{linkIndex + 1} = vertexData;
            end

%             if ~isempty(self.toolModelFilename)
%                 [ faceData, vertexData, plyData{self.model.n + 1} ] = plyread(self.toolModelFilename,'tri'); 
%                 self.model.faces{self.model.n + 1} = faceData;
%                 self.model.points{self.model.n + 1} = vertexData;
%                 toolParameters = load(self.toolParametersFilename);
%                 self.model.tool = toolParameters.tool;
%                 self.model.qlim = toolParameters.qlim;
%                 warning('Please check the joint limits. They may be unsafe')
%             end
            % Display robot
            self.model.plot3d(zeros(1,self.model.n),'noarrow','workspace',self.workspace);%'workspace',self.workspace instead of 'notiles'

            if isempty(findobj(get(gca,'Children'),'Type','Light'))
                camlight
            end  
            self.model.delay = 0;

            % Try to correctly colour the arm (if colours are in ply file data)
            for linkIndex = 0:self.model.n
                handles = findobj('Tag', self.model.name);
                h = get(handles,'UserData');
                try 
                    h.link(linkIndex+1).Children.FaceVertexCData = [plyData{linkIndex+1}.vertex.red ...
                                                                  , plyData{linkIndex+1}.vertex.green ...
                                                                  , plyData{linkIndex+1}.vertex.blue]/255;
                    h.link(linkIndex+1).Children.FaceColor = 'interp';
                catch ME_1
                    disp(ME_1);
                    continue;
                end
            end
        end         
    end
end