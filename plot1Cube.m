function [h,v,cubeVertexCount] = plot1Cube(cubePose, name)
    
    hold on;
    [f,v,data] = plyread(name,'tri');
    cubeVertexCount = size(v,1);
    vertexColours = [data.vertex.red, data.vertex.green, data.vertex.blue,] / 255;
    h = trisurf(f, v(:,1), v(:,2), v(:,3), 'FaceVertexCData', vertexColours);
    updatedPoints = [cubePose * [v, ones(cubeVertexCount,1)]']';
    h.Vertices = updatedPoints(:,1:3);

end

