%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Project           	  : Robotics Assignment 2
%
% Author           		  : Jorge Villanueva Salamero & Riccardo dalla
%
% Last Date Modified      : 06-07-2019
%
% Purpose           	  : Detection of the cubes positions, haviung the origin of coordinates in the centre of the robot
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [distance_x, distance_y] = detect_cubes()

	move_Robot = 0;	% if move_Robot == 1, the robot will move to the position of the objects(cubes) detected on the camera;

	clc
	close all

	% First We move the robot to one of the sides, just in case is between the Camera and the Cubes
	% Subsequently thhe Kinect Camera will capture a photo to then detect where the objects are.

	%rosinit('http://10.42.0.1:11311')
	jangsvc_ = rossvcclient('/dobot_magician/PTP/set_joint_angles'); jangmsg_ = rosmessage(jangsvc_);	% Set Joint Angles Variables
	z_ofset_moving_cube = + 0.035;
	dobotIkconReal( [0.2 -0.1 z_ofset_moving_cube], jangsvc_, 0 );                          		 	% Compute IkCon and Go To Cube
	pause(3);
	 

	%  We are going to detect the two features in the border of the image, to calculate knowing its real distance ( 2cm ), the conversion between pixels and cms 
	% ( this information will be used after to calculate the positions of the cubes with respect to the real Robot ( Dobot Magician ) 
 
		%open video input from kinect
		vid1 = videoinput('kinect',1);
		vid2 = videoinput('kinect',2);

		start([vid1 vid2]);
		ImColor = getsnapshot(vid1); 			% Get image to analyse
		Im = rgb2gray(ImColor); 				% Convert colored image (1080x1920x3) to gray scale image (1080x1920)
		our_image = detectSURFFeatures(Im);   	% Read image and detect interest points.
		figure;
		imshow(Im);
		title('10 Strongest Feature Points from Image');
		hold on;
		c = selectStrongest(our_image, 10);		% Select points with strongest metrics
		x = c.Location(:,1);
		
		[M,I] = max( x ); 						% Select most bottom right feature in the picture 
		
		[point_at_2cm_from_border, SecondI] = max(x(x<max(x))); % Select second most bottom right feature in the picture 
		border = c.Location(I,:);
		
		divide_by_this_number_to_get_cm = (border(1) - point_at_2cm_from_border(1)) / 2;	% Get Variable to convert to cm 
		
		plot(selectStrongest(our_image, 10));		% Plot strongest points




	% Now we are going to detect the cubes in the image
	
		thresholdValue = 150; 						%threshold value to detect objects
		binaryImage = Im < thresholdValue; 			% Bright objects will be chosen if you use >
		se = strel('square',60); 					%TO ADJUST	
		imClean = imopen(binaryImage,se); 			%performs morphological opening on the binary image imBinary with the structuring element se (squares)
		imClean = imfill(imClean,'holes');
		imClean = imclearborder(imClean);
		figure(3)
		imshow(imClean)
		labeledImage = bwlabel(imClean, 8); 		%label image
		imshow(labeledImage, []);
		coloredLabels = label2rgb (labeledImage, 'hsv', 'k', 'shuffle');
		imshow(coloredLabels); 						%show colored labels on image (NOT real colors!)
		axis image;
		blobMeasurements = regionprops(labeledImage, Im, 'all');
		numberOfBlobs = size(blobMeasurements, 1); %number of desired objects detected

		i=0;
		for k = 1 : numberOfBlobs   
			blobCentroid = blobMeasurements(k).Centroid; 	%position of objects (invert x)
			blobArea = blobMeasurements(k).Area; 			%area of objects in pixels    

			i = i + 1;
			positions(i,1:2) = round(blobCentroid); 		%return centroid for each blob
			r = ImColor(positions(k,2),positions(k,1),1);	% check color of each blob
			g = ImColor(positions(k,2),positions(k,1),2);
			b = ImColor(positions(k,2),positions(k,1),3);
			if (g < r && b < r) color(k) = 'r';
			else if (r < g && b < g) color(k) = 'g';
			else if (r < b && g < b) color(k) = 'b';
				end
				end
			end
		end


		% Change the postion of the cubes, having as the new origin, the center of the robot
		
			% Distance from the cubes to points of the border ( We know the distance from this points to the robot in the real life )
			for i = 1 : size(positions) 
			   distance_x(1,i) = ( border(1,1) - positions(i,1) )/ (divide_by_this_number_to_get_cm );
			   distance_y(1,i) =  (border(1,2) - positions(i,2) )/ (divide_by_this_number_to_get_cm );
			end

			% Substract the distance from the points in the border to the centre of the robot
			for i = 1 : size(positions) 
			   distance_x(1,i) = 11.95 - distance_x(i); % to the center of the sheet of paper
			   distance_y(1,i) = 32.5 - distance_y(i);  % 19 +6 +7.5
			end

			% Convert to meters
			for i = 1 : size(positions) 
			   distance_x(1,i) =  ( distance_x(i) / 100 );
			   distance_y(1,i) = distance_y(i) / 100;
			end

			if ( move_Robot == 1 )
				 % Go To the cubes detected
				for i = 1 : size(positions) 
					 dobotIkconReal( [distance_x(i ) distance_y(i)  z_ofset_moving_cube], jangsvc_, 0 );                          
					 dobotIkconReal( [distance_x(i ) distance_y(i)  z_ofset_suctionLL], jangsvc_, 0 );    
				 end
			end
end			


