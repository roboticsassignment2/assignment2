function move_simulation_robot_and_cube(q, robot, steps, h_object, v_object, cubeVertexCount_object)

z_ofset_cube = -0.02;

s = lspb(0,1,steps);
qMatrix = nan(steps,5);
q_old =  robot.model.getpos();

for i = 1:steps
    qMatrix(i,:) = (1-s(i))*q_old + s(i)*  q;  
end

for i=1:steps   
    robot.model.animate(qMatrix(i,:));

    auxq = robot.model.getpos();
 
    robot_pose = robot.model.fkine( auxq  );
    robot_pose = transl ( robot_pose(1:3,4 ))
    robot_pose (3,4) = robot_pose (3,4) + z_ofset_cube;
    % Transform the vertices
    updatedPoints = [robot_pose * [v_object,ones(cubeVertexCount_object,1)]']';

    % Update the mesh vertices in the patch handle
    h_object.Vertices = updatedPoints(:,1:3);

    drawnow
    %if (checkButton(a) == 1) break;
    %end
end

end