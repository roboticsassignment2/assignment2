function pickAndPlaceReal()
clc 
close all

z_ofset_moving_cube = + 0.035;

z_ofset_suctionLL = - 0.052;

z_ofset_dropping_L1 = -0.045;
z_ofset_dropping_L2 = -0.018;

redCube1 =   [0.2 0.2 0];
redCube2 =   [0 0.3 0];
greenCube1 = [0.1 0.2 0];
greenCube2 = [0.15 0.25 0];
blueCube1 =  [0 0.2 0];
blueCube2 =  [0.1 0.25 0];

center_redCube1 = redCube1 + [0.01, 0.012, 0] ;
center_redCube2 = redCube2 + [0.011, 0.01, 0] ;
center_greenCube1 = greenCube1 + [0.01, 0.01, 0] ;
center_greenCube2 = greenCube2 + [0.008, 0.01, 0] ;
center_blueCube1 = blueCube1 + [0, 0.01, 0] - [0.002, 0, 0];
center_blueCube2 = blueCube2 + [0.011, 0.014, 0] 


% Level 1
final_redCube1_moving =   [0.287  0  z_ofset_moving_cube 0];
final_redCube1_dropping =   [0.287  0  z_ofset_dropping_L1 0];
final_redCube2_moving = [0.26 0  z_ofset_moving_cube 0 ];
final_redCube2_dropping = [0.26  0  z_ofset_dropping_L1 0 ];
final_greenCube1_moving = [0.23 0  z_ofset_moving_cube 0 ];
final_greenCube1_dropping = [0.23  0  z_ofset_dropping_L1 0 ];

% Level 2
final_greenCube2_moving = [0.245 0  z_ofset_moving_cube 0 ];
final_greenCube2_dropping = [0.245 0  z_ofset_dropping_L2 0 ];
final_blueCube1_moving = [0.269 0  z_ofset_moving_cube 0 ];
final_blueCube1_dropping = [0.269  0  z_ofset_dropping_L2 0 ];

% Level 3
final_blueCube2_moving = [0.259 0  z_ofset_moving_cube 0 ];
final_blueCube2_dropping = [0.259  0  0 0 ];

%rosinit('http://10.42.0.1:11311')
cartsvc_ = rossvcclient('/dobot_magician/PTP/set_cartesian_pos');
cartmsg_ = rosmessage(cartsvc_);
suctioncupsvc_ = rossvcclient('/dobot_magician/end_effector/set_suction_cup');
suctioncupmsg_ = rosmessage(suctioncupsvc_);

cartsvc_ = rossvcclient('/dobot_magician/PTP/set_cartesian_pos'); cartmsg_ = rosmessage(cartsvc_);
jangsvc_ = rossvcclient('/dobot_magician/PTP/set_joint_angles'); jangmsg_ = rosmessage(jangsvc_);



cartsvc_ = rossvcclient('/dobot_magician/PTP/set_cartesian_pos'); cartmsg_ = rosmessage(cartsvc_);
jangsvc_ = rossvcclient('/dobot_magician/PTP/set_joint_angles'); jangmsg_ = rosmessage(jangsvc_);

suctioncupmsg_.IsEndEffectorEnabled=1;
suctioncupmsg_.EndEffectorState=1;
suctioncupsvc_.call(suctioncupmsg_);

% Red 1
cartmsg_.TargetPoints= [center_redCube1(1) center_redCube1(2) center_redCube1(3)+z_ofset_moving_cube 0]; cartsvc_.call(cartmsg_);
suctioncupmsg_.EndEffectorState=1; suctioncupsvc_.call(suctioncupmsg_);
cartmsg_.TargetPoints= [center_redCube1(1) center_redCube1(2) center_redCube1(3)+z_ofset_suctionLL 0];  cartsvc_.call(cartmsg_)
pause(4);
cartmsg_.TargetPoints= [center_redCube1(1) center_redCube1(2) center_redCube1(3)+z_ofset_moving_cube 0]; cartsvc_.call(cartmsg_);
cartmsg_.TargetPoints= [final_redCube1_moving]; cartsvc_.call(cartmsg_);
cartmsg_.TargetPoints= final_redCube1_dropping; cartsvc_.call(cartmsg_);

b = distance(0,0, final_redCube1_moving(1), final_redCube1_moving(2));
c = distance(0,0, center_redCube1(1), center_redCube1(2));

jangmsg_.TargetPoints = [0,   1.1223, 0.7815, 0.10 ]; jangsvc_.call(jangmsg_);
pause(6);
suctioncupmsg_.EndEffectorState=0; suctioncupsvc_.call(suctioncupmsg_);
pause(3);
cartmsg_.TargetPoints= [final_redCube1_moving]; cartsvc_.call(cartmsg_);

% Red 2
cartmsg_.TargetPoints= [center_redCube2(1) center_redCube2(2) center_redCube2(3)+z_ofset_moving_cube 0]; cartsvc_.call(cartmsg_)
suctioncupmsg_.EndEffectorState=1; suctioncupsvc_.call(suctioncupmsg_);
cartmsg_.TargetPoints= [center_redCube2(1) center_redCube2(2) center_redCube2(3)+z_ofset_suctionLL 0]; cartsvc_.call(cartmsg_)
pause(4);
cartmsg_.TargetPoints= [center_redCube2(1) center_redCube2(2) center_redCube2(3)+z_ofset_moving_cube 0]; cartsvc_.call(cartmsg_)
cartmsg_.TargetPoints= final_redCube2_moving; cartsvc_.call(cartmsg_);
jangmsg_.TargetPoints = [0,   0.4132, 0.4103, 0.18 ]; jangsvc_.call(jangmsg_);
cartmsg_.TargetPoints= final_redCube2_dropping; cartsvc_.call(cartmsg_);
pause(8);
suctioncupmsg_.EndEffectorState=0; suctioncupsvc_.call(suctioncupmsg_);
pause(3);
cartmsg_.TargetPoints= final_redCube2_moving; cartsvc_.call(cartmsg_);

% Green cube 1
cartmsg_.TargetPoints= [center_greenCube1(1) center_greenCube1(2) center_greenCube1(3)+z_ofset_moving_cube 0]; cartsvc_.call(cartmsg_);
suctioncupmsg_.EndEffectorState=1; suctioncupsvc_.call(suctioncupmsg_);
cartmsg_.TargetPoints= [center_greenCube1(1) center_greenCube1(2) center_greenCube1(3)+z_ofset_suctionLL 0];  cartsvc_.call(cartmsg_)
pause(4);
cartmsg_.TargetPoints= [center_greenCube1(1) center_greenCube1(2) center_greenCube1(3)+z_ofset_moving_cube 0]; cartsvc_.call(cartmsg_);
cartmsg_.TargetPoints= final_greenCube1_moving; cartsvc_.call(cartmsg_);
jangmsg_.TargetPoints = [0,   0.3537, 0.5888, 0.18 ]; jangsvc_.call(jangmsg_);
cartmsg_.TargetPoints= final_greenCube1_dropping; cartsvc_.call(cartmsg_);
pause(6);
suctioncupmsg_.EndEffectorState=0; suctioncupsvc_.call(suctioncupmsg_);
pause(3);
cartmsg_.TargetPoints= final_greenCube1_moving; cartsvc_.call(cartmsg_);

% Green cube 2
cartmsg_.TargetPoints= [center_greenCube2(1) center_greenCube2(2) center_greenCube2(3)+z_ofset_moving_cube 0]; cartsvc_.call(cartmsg_);
suctioncupmsg_.EndEffectorState=1; suctioncupsvc_.call(suctioncupmsg_);
cartmsg_.TargetPoints= [center_greenCube2(1) center_greenCube2(2) center_greenCube2(3)+z_ofset_suctionLL 0];  cartsvc_.call(cartmsg_)
pause(4);
cartmsg_.TargetPoints= [center_greenCube2(1) center_greenCube2(2) center_greenCube2(3)+z_ofset_moving_cube 0]; cartsvc_.call(cartmsg_);
cartmsg_.TargetPoints= final_greenCube2_moving; cartsvc_.call(cartmsg_);
jangmsg_.TargetPoints = [0,   0.6619,  0.2861, 0.18 ]; jangsvc_.call(jangmsg_);
cartmsg_.TargetPoints= final_greenCube2_dropping; cartsvc_.call(cartmsg_);
pause(9);
suctioncupmsg_.EndEffectorState=0; suctioncupsvc_.call(suctioncupmsg_);
pause(3);
cartmsg_.TargetPoints= final_greenCube2_moving; cartsvc_.call(cartmsg_);

% % blue cube 1
cartmsg_.TargetPoints= [center_blueCube1(1) center_blueCube1(2) center_blueCube1(3)+z_ofset_moving_cube 0]; cartsvc_.call(cartmsg_);
suctioncupmsg_.EndEffectorState=1; suctioncupsvc_.call(suctioncupmsg_);
cartmsg_.TargetPoints= [center_blueCube1(1) center_blueCube1(2) center_blueCube1(3)+z_ofset_suctionLL 0];  cartsvc_.call(cartmsg_)
pause(4);
cartmsg_.TargetPoints= [center_blueCube1(1) center_blueCube1(2) center_blueCube1(3)+z_ofset_moving_cube 0]; cartsvc_.call(cartmsg_);
cartmsg_.TargetPoints= final_blueCube1_moving; cartsvc_.call(cartmsg_);
jangmsg_.TargetPoints = [0,   0.5777, 0.4816, 0.18 ]; jangsvc_.call(jangmsg_);
cartmsg_.TargetPoints= final_blueCube1_dropping; cartsvc_.call(cartmsg_);
pause(6);
suctioncupmsg_.EndEffectorState=0; suctioncupsvc_.call(suctioncupmsg_);
pause(3);
cartmsg_.TargetPoints= final_blueCube1_moving; cartsvc_.call(cartmsg_);

% blue cube 2
cartmsg_.TargetPoints= [center_blueCube2(1) center_blueCube2(2) center_blueCube2(3)+z_ofset_moving_cube 0]; cartsvc_.call(cartmsg_);
suctioncupmsg_.EndEffectorState=1; suctioncupsvc_.call(suctioncupmsg_);
cartmsg_.TargetPoints= [center_blueCube2(1) center_blueCube2(2) center_blueCube2(3)+z_ofset_suctionLL 0];  cartsvc_.call(cartmsg_)
pause(4);
cartmsg_.TargetPoints= [center_blueCube2(1) center_blueCube2(2) center_blueCube2(3)+z_ofset_moving_cube 0]; cartsvc_.call(cartmsg_);
cartmsg_.TargetPoints= final_blueCube2_moving; cartsvc_.call(cartmsg_);
jangmsg_.TargetPoints = [0,   0.5760, 0.5610, 0.18 ]; jangsvc_.call(jangmsg_);
cartmsg_.TargetPoints= final_blueCube2_dropping; cartsvc_.call(cartmsg_);
pause(6);
suctioncupmsg_.EndEffectorState=0; suctioncupsvc_.call(suctioncupmsg_);
pause(3);
cartmsg_.TargetPoints= final_blueCube2_moving; cartsvc_.call(cartmsg_);
pause(3);


c = 5;    


end

